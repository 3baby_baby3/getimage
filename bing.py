
import os
import urllib.parse
import string
import random
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome import options

CurrentDir = os.getcwd()
os.mkdir(CurrentDir + "/imageBing")

"""
@Author
Hayato
 
@Environment
OS: Linux(今回はUbuntu14.04LTS)
言語: python3.4.3
"""

# Google Chromeを使用する場合
# /opt/以下にバイナリを入れているため引数なしでも起動可能になっています
# bingの場合はユーザエージェントを変更しないと画像検索がうまくできなかった
# 起動時は別途ダウンロードが必要です
# 最終的にはPhantom JSに移行します
options = options.Options()
options.add_argument('--user-agent="Mozilla/5.0 (Linux; U; Android 2.3.3; ja-jp; SC-02C Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"')
browser = webdriver.Chrome(chrome_options=options)

class GetImageBing():

    def extract_info(self,source):
        """
        画像URLを抽出し、ダウンロード 

        :param source: ブラウザが現在表示しているソースコード
        """
        string_range = 8
        soup = BeautifulSoup(source,"lxml")
        for a in soup.find_all("div",attrs={"class":"img_cont hoff"}):
            for b in a.find_all("img"):
                image = b.attrs["src"]
                # ランダムに8文字以内で文字生成
                name = ''.join([random.choice(string.ascii_letters + string.digits) for i in range(string_range)])
                # DEBUG
                print (image)
                print (name)
                # 画像のダウンロード
                # 現段階ではjpgで固定にしているので他のフォーマットだった場合はうまく表示できない可能性があります
                urllib.request.urlretrieve(image,CurrentDir + "/imageBing/" + name + ".jpg")

    def search(self):
        """
        指定したキーワードでBing画像検索をする 

        """
        # URL整形のときのパラメータ
        count = 28
        page = 1
        # DEBUG
        # page = 600

        print ("Input keywords: ",end="")
        keywords = input()
        #keywords = "猫"
        # URLエンコード
        quoted = urllib.parse.quote(keywords)
        # 適切なURLに整形
        url_fmt = "https://www.bing.com/images/async?q={}&first={}&count={}&relp=28&mmasync=1"
        URL = url_fmt.format(quoted, page, count)
        # 整形したURLにアクセス
        browser.get(URL)
        # ブラウザが現在アクセスしているソースコード
        source = browser.page_source
        while True:
            try:
                self.extract_info(source)
                url_fmt = "https://www.bing.com/images/async?q={}&first={}&count={}&relp=28&mmasync=1"
                # 2ページ以降に遷移
                page = page + count
                URL = url_fmt.format(quoted, page, count)
                browser.get(URL)
                # 2ページ以降のソースコード
                source = browser.page_source
                # 終了判定が思いつかなかったので適当なところで終わらせることにしました
                if page >= 700:
                    print ("収集を終了します")
                    print ("Thank you")
                    browser.close()
                    exit()
            except ConnectionResetError as e:
                print (e)
                print ("収集を終了します")
                print ("Thank you")
                browser.close()

    def main(self):
        """
        指定キーワードで画像検索を行い検索結果全ての画像を収集する

        """
        source = self.search()


