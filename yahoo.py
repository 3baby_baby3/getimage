
import os
import urllib.parse
import string
import random
import time
from bs4 import BeautifulSoup
from selenium import webdriver

URL = "https://search.yahoo.co.jp/image/search?p="
CurrentDir = os.getcwd()
os.mkdir(CurrentDir + "/imageYahoo")
path = CurrentDir + "/imageYahoo/"

"""
@Author
Hayato

@Environment
OS: Linux(今回はUbuntu14.04LTS)
言語: python3.4.3
"""

# Google Chromeを使用する場合
# /opt/以下にバイナリを入れているため引数なしでも起動可能になっています
# 起動時は別途ダウンロードが必要です
# 最終的にはPhantom JSに移行します
browser = webdriver.Chrome()

class GetImageYahoo():

    def extract_info(self,source):
        """
        画像URLを抽出し、ダウンロード 

        :param source: ブラウザのソースコード
        """
        string_range = 8
        soup = BeautifulSoup(source,"lxml")
        for a in soup.find_all("p",attrs={"class":"tb"}):
            for b in a.find_all("img"):
                image = b.attrs["src"]
                # ランダムに8文字以内で文字生成
                name = ''.join([random.choice(string.ascii_letters + string.digits) for i in range(string_range)])
                # DEBUG
                print (image)
                print (name)
                # 画像のダウンロード
                # 現段階ではjpgで固定にしているので他のフォーマットだった場合はうまく表示できない可能性があります
                urllib.request.urlretrieve(image,path + name + ".jpg")

    def next_page(self):
        """
        2ページ以降にアクセスを行い画像URLを抽出し、ダウンロード

        """
        number = 21
        # DEBUG
        #number = 1000
        string_range = 8
        while True:
            # 2ページ以降のURLを生成
            next_page_URL = self.URL + "&b=" + str(number)
            # 生成されたURLにブラウザでアクセス
            browser.get(next_page_URL)
            time.sleep(5)
            # 生成されたURLのソースコード
            next_page_source = browser.page_source
            soup = BeautifulSoup(next_page_source,"lxml")
            for a in soup.find_all("p",attrs={"class":"tb"}):
                for b in a.find_all("img"):
                    image = b.attrs["src"]
                    # ランダムに8文字以内で文字生成
                    name = ''.join([random.choice(string.ascii_letters + string.digits) for i in range(string_range)])
                    # DEBUG
                    print (image)
                    print (name)
                    # 画像のダウンロード
                    # 現段階ではjpgで固定にしているので他のフォーマットだった場合はうまく表示できない可能性があります
                    urllib.request.urlretrieve(image,path + name + ".jpg")
            # 終了判定
            if "一致" or "ただいま" in soup.text:
                print ("==================")
                print ("収集を終了します")
                print ("thank you")
                print ("==================")
                browser.close()
                exit()
            number = number + 20

    def search(self):
        """
        指定したキーワードでYahoo画像検索をする 

        :return ブラウザのソースコード
        """
        print ("Input keywords: ",end="")
        keywords = input()
        # URLエンコード
        quoted = urllib.parse.quote(keywords)
        print (URL + quoted)
        # self.URLに使用しているURLを代入
        self.URL = URL + quoted
        browser.get(URL + quoted)
        # スクレイピングの都合上簡易版に変更
        browser.find_element_by_xpath("//*[@id='modeChange']/a").click()
        source = browser.page_source
        return source

    def main(self):
        """
        指定キーワードで画像検索を行い検索結果全ての画像を収集する

        """
        source = self.search()
        self.extract_info(source)
        self.next_page()
